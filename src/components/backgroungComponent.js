import React from 'react';
import { Button, Card, CardContent, Container, Grid,  ListItem, ListItemIcon, ListItemText, TextField, Typography } from '@material-ui/core';

class Main extends React.Component{
	
	constructor(props){
		super(props);
		this.state={
			expense : 0,
			total : 0,
			text : '',
			amount : '',
			isAdd : '',
			balance: 0,
			list:[] 
		}
	}

	onChangedHangle = (event) =>{
		this.setState({[event.target.name] : event.target.value});
	}

	  // Function to delete item from list use id to delete 
	  deleteItem = (key)=>{ 
		const list = [...this.state.list]; 
		
		
		// Filter values and leave value which we need to delete 
		const updateList = list.filter(item => item.id !== key,

			
			); 
	  
		// Update list in state 
		this.setState({ 
		  list:updateList, 
		})
	}

	amountTotal = (event)=>{
		event.preventDefault();
		
		const amount = this.state.amount;
		if(amount > 0 ){
			this.setState({
					
				total: Number(this.state.amount) + Number(this.state.total),
				balance :  Number(this.state.amount) + Number(this.state.balance)
			})	
		}
		if(amount < 0 ){
			
			this.setState({
				expense: Number(this.state.amount) + Number(this.state.expense),
				balance :  Number(this.state.amount) + Number(this.state.balance)
			})	
		}
		
		if(this.state.text !== '' && this.state.amount !== '' ){ 
			const text = { 
		
			  // Add a random id which is used to delete 
			  id :  Math.random(), 
		
			  // Add a user value to list 
			  amount : this.state.amount,
			  value : this.state.text 
			}; 
			const list = [...this.state.list]; 
			list.push(text); 
		
			// reset state 
			this.setState({ 
				list, 
				text:" ",
				amount: " ",
				
			  })

			  console.log(list)
		}
	}
	
	render(){
		// console.log(this.state.list);

		return (
			
			<Container maxWidth="sm"  style={{ backgroundColor:'#E7E7E7', borderRadius:'10px' ,  marginLeft: '40' , opacity:'95%'}} >
				<Grid sm={12} >

			
			<Grid  spacing={4} style={{  marginLeft: '40' }}>
						<Grid cellHeight={160}  cols={3}>
						<h1>Expense Tracker</h1>
						</Grid>

						<Grid sm={4}>
						<h4 style={{ marginBottom: '-17px' ,fontFamily: 'sans-serif' }}>Your Balance</h4>
						<h2>Rs {this.state.balance}</h2>
						</Grid>

			<Grid sm={12}>
			<Card >
				<CardContent>
					<Grid container>
						<Grid sm={6} >
						<Typography >
							<div style={{ marginLeft: '37px' }}>

							
							<b>INCOME</b> <br />
							<span  style={{  color: 'green' }}>{this.state.total}</span>
							</div>
							</Typography>
						</Grid>
						<Grid sm={2} >
						<Typography  >
						<div style={{ borderLeft: '2px solid gray'  , height:'60px'}}></div>
							</Typography>
						</Grid>
					
						<Grid sm={4} >
						<Typography  >
						<b>EXPENSE</b> <br />
							<span  style={{  color: 'red' }}>{this.state.expense}</span>
							</Typography>
						</Grid>
					
					</Grid>	
				</CardContent>	
			</Card>
			</Grid>
			<Grid sm={12}>
			<h3 style={{fontFamily: 'sans-serif' }}>History</h3>
			<hr />
			</Grid>
			
		<Grid sm={12}>

		
			{this.state.list.map(item => {return( 
				item.amount > 0 ?
				<ListItem dense button style={{backgroundColor:'#BBCB75' , borderRight: '8px solid Green'}} onClick = { () => this.deleteItem(item.id) }>
				<ListItemText>{item.value}</ListItemText>
				<ListItemIcon style={{alignContent:'right'}}>
				<span>RS {item.amount}</span>
			
				</ListItemIcon>

				
				<br />
				</ListItem>
				:
				<ListItem dense button style={{backgroundColor:'#FFD5D5' , borderRight: '8px solid red'} } onClick = { () => this.deleteItem(item.id) }>
				<ListItemText>{item.value}</ListItemText>
				<ListItemIcon style={{alignContent:'right'}}>
				<span>RS {item.amount}</span>
				</ListItemIcon>
				<br />
				</ListItem>	
				

		)})}
		</Grid>
		
		<Grid sm={12}>
		<form >
			<h3>Add new Transaction</h3>
			<hr />
			
			<TextField style={{width:'69ch'}} id="outlined-basic" name="text" label="Text" variant="outlined"  onChange={(e)=>this.onChangedHangle(e)}/>
			<br />
			<br />
			
			<TextField style={{width:'69ch'}} type="number"  id="outlined-basic" name="amount"  label="Amount (negative - expense , positive- income)" variant="outlined" onChange={(e)=>this.onChangedHangle(e)}/>
			
			
			</form>
			<br />
			<br />
			<Button style={{width:'69ch', marginBottom: '26px'}} variant="contained" color="primary" href="#contained-buttons" onClick={(e)=>this.amountTotal(e)}>
				Add Transaction
			</Button>
			</Grid>				
		
			</Grid>
			</Grid>
	  </Container>

	
		);
	}

}

export default Main